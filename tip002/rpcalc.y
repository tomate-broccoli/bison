/* Reverse Polish Calculator */
%{
    #include <stdio.h>
    #include <math.h>
    int yylex(void);
    void yyerror (char const *);
%}

%define api.value.type {double}
%token NUM

%% /* grammar rules */

input:
    %empty
  | input line
;

line:
    '\n'
  | exp '\n' { printf("\t%.10g\n", $1); }
;

exp:
    NUM      /* { $$ = $1 } */
  | exp exp '+' { $$ = $1 + $2; } 
  | exp exp '-' { $$ = $1 - $2; } 
  | exp exp '*' { $$ = $1 * $2; } 
  | exp exp '/' { $$ = $1 / $2; } 
  | exp exp '^' { $$ = pow ($1 , $2); }
  | exp 'n'     { $$ = - $1; } 
;
%%

/* Additional C code */
#include <ctype.h>
#include <stdlib.h>

int yylex(void) {
    int c = getchar();
    /* skip white space */
    while((c == ' ') || c=='\t')
        c = getchar();
    /* gather numbers */
    if(c == '.' || isdigit(c)) {
        ungetc(c, stdin);
        scanf("%lf", &yylval);
        return NUM;
    }
    /* EOF? */
    if(EOF == c) {     // yes
        return YYEOF;  // 0;
    }
    /* return all other single characters */
    return c;
}

int main(void) {
   return yyparse();
}

#include <stdio.h>

void yyerror(char const *s){
    printf("%s\n", s);
}
